Wheel of Horror
===============

Should be pronounced with a proper british accent 
and with a high pitch in the end. Rolling r's. Have a look 
in the "Media" folder for a recording in case you are not sure. 

This is a simple embedded project that uses
MSP430 G2231 microcontroller to control 16 LEDs. 
There is also a couple of programmable buttons
and ports for daisychaining together several 
of the boards so that more people can join in
if they have their own board (They need to solder it themselves).

Rules
-----
Wheel of Horror is a drinking game. If you only have one board, 
it functions as a wheel of fortune, but there is no fortune for 
the winner, 'cos he has to drink a shot of something. 

 
Hardware
--------
To program the microcontroller, a TI Lunchpad is used. In fact, 
the the G2231 is the microcontroller that comes with the Launchpad. 
To control the LEDs, two serial to parallel shift registers are used. 
Other than that, its just a collection of your favourite colored LEDs. 

Firmware
--------
In the firmware folder, the Makefile and the .c-file is all you really need. 
Just write "make" and then "make install" to upload new firmware. 
You will need an msp430-gcc toolchain for your host operating system. 
For MAC, have a look at   
http://processors.wiki.ti.com/index.php/MSP430_LaunchPad_Mac_OS_X  




