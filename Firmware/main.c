/******************************************************************************
Wheel of horror

Prototcol: Looking at the board, the led line is on the bottom.
Bottom Right pin is 		p1.6 -> RIGHT_IN
Pin two on the right is 	p1.7 -> RIGHT_OUT
Bottom left pin is 			p1.5 -> LEFT_OUT
Second pin on the right is  p1.4 -> LEFT_IN

* uC: MSP430G2231, the one that comes with the Launchpad from TI.
*
* Copyright (C) 2011  Intelligent Agent AS <software@iagent.no>
* You may use all or parts of this code any way you want. You can even remove
* the copyright notice and these conditions. There is NO WARRANTY, to the
* extent permitted by law.
*
* 15.05.2011 - Elias Bakken <elias@iagent.no>
*******************************************************************************/

#include <io.h>
#include <signal.h>


#define SER BIT0
#define SRCLR BIT1
#define SCK BIT2
#define RCK BIT3
#define LEFT_IN BIT4
#define LEFT_OUT BIT5
#define RIGHT_IN BIT6
#define RIGHT_OUT BIT7
#define LEFT_BUTTON BIT6
#define RIGHT_BUTTON BIT7

#define START_DELAY 20
#define DELTA_DELAY 20
#define DELAY_STEPS 7

int seedx = 0;

void write_ser(int ser);
void toggle_sck();
void toggle_rck();
void clear_data();

void set_seed();
int random(void);
void delay_ms(unsigned int n);			// Delay milliseconds.
static void __inline__ _brief_pause(register unsigned int n);
void delay_us(unsigned int n);
void startup_sequence();


int lo = 0;
int hi = 15;
int main(void){
  WDTCTL = WDTPW + WDTHOLD; 								// Stop WDT
  P1DIR = SER  | SCK | RCK | SRCLR | RIGHT_OUT | LEFT_OUT;  // Pins for output
  P1REN |= RIGHT_IN | LEFT_IN;								// Pull-up on inputs and outputs
  P1OUT = 0xFF;												// Pull UP, not down
  P1SEL = 0;												// I/O pins
  P2SEL = 0;												// I/O pins
  P2DIR = ~LEFT_BUTTON & ~RIGHT_BUTTON;

  int direction = 1;
  int i = lo;
  int j;
  int delay_time = START_DELAY;

  startup_sequence();

  set_seed();

  int cont = 0;

  while(1){


	  while(1){
			  if(left_button_pressed() || right_button_pressed()){
				  delay_ms(1);
				  while(left_button_pressed() || right_button_pressed()); // Wait for
				  delay_time = START_DELAY;
				  i = 8;
				  direction = 1;
				  break;
			  }
			  else if(check_left()){ 		// Left pin has gone low, packet is pending
				  delay_time = read_left(); // Read the package from the left opponent
				  write_left(delay_time);	// Write the package back to the opponent to acknowledge
				  direction = 1;
				  i = lo;
				  break;
			  }
			  else if(check_right()){
				  delay_time = read_right();
				  write_right(delay_time);
				  i = hi;
				  direction = 0;
				  break;
			  }
	  }
	  while(1){
		  write_ser(1<<i);		// Output the led number
		  if(direction == 1)
			  i++;
		  else
			  i--;

		  if(i == hi+1){					// We are now on the right side.
			  write_right(delay_time); 		// Try writing a package to the right.
			  for(j=0; j<delay_time/2; j++){ 	// If the package is received, the RX pin is pulled low
				  if(check_right()){
					  while(check_right()); // Wait for the package to be received
					  cont = 1;
					  break;
				  }
				  delay_ms(1);
			  }
			  // If we reach this point, the package was not acknowledged, so we change direction
			  direction = 0;
		  }
		  else if(i==lo){					// We are now on the left side.
			  write_left(delay_time);		// Try to write a package to the left.
			  for(j=0; j<delay_time/2; j++){	// If the package is received, the RX pin is pulled low
				  if(check_left()){
					  while(check_left());	// Wait for the package to be received
					  cont = 1;
					  break;
				  }
				  delay_ms(1);
			  }
			  // If we reach this point, the package was not acknowledged, so we change direction
			  direction = 1;
		  }
		  // If cont is set, a package has been acknowledged and we can stop.
		  if(cont){
			  cont = 0;
			  write_ser(0);
			  break;
		  }
		  else if(i != lo && i != hi)
			  delay_ms(delay_time);

		  // By using a random, we either do nothing or slow down.
		 if(random() > 0xFFFFF000)
			  delay_time += DELTA_DELAY;

		  // If the delay is long enough, we stop completely
		  if(delay_time >= START_DELAY+DELAY_STEPS*DELTA_DELAY){
			  for(j=0; j<10; j++){
				  delay_ms(100);
				  write_ser(1<<i);
				  delay_ms(100);
				  write_ser(0x0000);
			  }
			  delay_time = START_DELAY;
			  i = 0;
			  direction = 1;
			  break;
		  }
	   }
  }
  return 0;
}

// Writes the data in ser to the shift buffers
void write_ser(int ser){
	int i;

	// Clear the data currently in the shift buffers
	clear_data();

	// Clock out the data
	for(i=0; i<16; i++){

		//Set the output on the pin
		if(ser & (0x01<<i))
			P1OUT |= SER;
		else
			P1OUT &= ~SER;

		// Toggle the clock
		toggle_sck();
	}
	// switch data to output buffer
	toggle_rck();
}

// Clock in the data on pin SER
void toggle_sck(){
	P1OUT |= SCK;
	P1OUT &= ~SCK;
}

// Clock the data in the serial buffer to the output
void toggle_rck(){
	P1OUT |= RCK;
	P1OUT &= ~RCK;
}

// Clear the current serial data
void clear_data(){
	P1OUT &= ~SRCLR;
	P1OUT |= SRCLR;
}



// Make a small pause.
static void __inline__ _brief_pause(register unsigned int n)
{
    __asm__ __volatile__ (
                "1: \n"
                " dec      %[n] \n"
                " jne      1b \n"
        : [n] "+r"(n));

}

void delay_us(unsigned int n){
	_brief_pause(n);
}
// Delay n milliseconds
void delay_ms(unsigned int n){
	do{
		_brief_pause(333);
	}while(n--);
}

//Generate a random 32 bit number
int random(){
	seedx = 1103515245 * seedx + 12345;
	return seedx & 0xFFFFFFFF;
}


void set_seed(){
	  ADC10CTL1 = INCH_10 + ADC10DIV_3;        						 // Temp Sensor ADC10CLK/4
	  ADC10CTL0 = SREF_1 + ADC10SHT_3 + REFON + ADC10ON + ADC10IE;
	  delay_ms(10);                     							// Wait for ADC Ref to settle
	  ADC10CTL0 |= ENC + ADC10SC;               					// Sampling and conversion start
	  seedx = ADC10MEM;
}

// Check if left pin has gone low
int check_left(){
	return (((P1IN & LEFT_IN) > 0) ? 0 : 1);
}

// Check if right pin has gone low
int check_right(){
	return (((P1IN & RIGHT_IN) > 0) ? 0 : 1);
}

// Write a value to the left opponent
void write_left(int value){
	P1OUT &= ~LEFT_OUT;		// Pull pin low
	delay_ms(value/2);		// Wait the delay time
	P1OUT |= LEFT_OUT;		// set the pin high
}

// Write a value to right opponent
void write_right(int value){
	P1OUT &= ~RIGHT_OUT;
	delay_ms(value/2);
	P1OUT |= RIGHT_OUT;
}

// Count the number of ms the pin is pulled low
int read_left(){
	int delay = 0;
	while(check_left()){
		delay++;
		delay_ms(1);
	}
	delay *= 2;
	// Discretisize the delays
	int del = START_DELAY;
	while(del-(START_DELAY/2) < delay && del-(START_DELAY/2) < START_DELAY + (DELAY_STEPS*DELTA_DELAY))
		del += DELTA_DELAY;

	return del;
}

// Count the number of ms the pin is pulled low
int read_right(){
	int delay = 0;
	while(check_right()){
		delay++;
		delay_ms(1);
	}
	delay *= 2;
	// Discretisize the delays
	int del = START_DELAY;
	while(del-(START_DELAY/2) < delay && del-(START_DELAY/2) < START_DELAY + (DELAY_STEPS*DELTA_DELAY))
		del += DELTA_DELAY;

	return del;
}

// return true if left button is pressed. Button has a pull-up
int left_button_pressed(){
	return ((P2IN & LEFT_BUTTON) == 0) ? 1 : 0;
}

// Return true if right button is pressed. Button has a pull-up
int right_button_pressed(){
	return ((P2IN & RIGHT_BUTTON) == 0) ? 1 : 0;
}


void startup_sequence(){
	write_ser((1<<0)+(1<<1)+(1<<2)+(1<<13)+(1<<14)+(1<<15));
	delay_ms(100);
	write_ser((1<<3)+(1<<12));
	delay_ms(100);
	write_ser((1<<4)+(1<<11));
	delay_ms(100);
	write_ser((1<<5)+(1<<10));
	delay_ms(100);
	write_ser((1<<6)+(1<<9));
	delay_ms(100);
	write_ser((1<<7)+(1<<8));
	delay_ms(100);
	write_ser(0);
}
